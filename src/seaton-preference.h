/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SEATON_PREFERENCE_H__
#define __SEATON_PREFERENCE_H__

#include <glib.h>
#include <glib-object.h>
#include "sqlite3.h"
#include <gio/gio.h>

G_BEGIN_DECLS

/**
 * SEATON_PREFERENCE_APP_INTERNAL_DB:
 *
 * App-specific internal DB that is visible across all multiusers.
 */
#define SEATON_PREFERENCE_APP_INTERNAL_DB 	0
/**
 * SEATON_PREFERENCE_APP_USER_DB:
 *
 * DB that is visible for particular user (active).
 */
#define SEATON_PREFERENCE_APP_USER_DB 		1
/**
 * SEATON_PREFERENCE_SERVICE_DB:
 *
 * DB which needs to be created by service.
 */
#define SEATON_PREFERENCE_SERVICE_DB 			2


/**
 * SeatonErrorResults:
 * @SEATON_PREFERENCE_PM_OK: Successful result
 * @SEATON_PREFERENCE_PM_BBE: Error-Bad Back-End
 * @SEATON_PREFERENCE_PM_BDS: Bad Data Store
 * @SEATON_PREFERENCE_PM_KNF: Key Not Found
 * @SEATON_PREFERENCE_PM_NMS: No More Space
 * @SEATON_PREFERENCE_PM_DSTB: Data Size Too Big
 * @SEATON_PREFERENCE_PM_OOR: Out Of Range
 * @SEATON_PREFERENCE_PM_BLOCKED: Data store is blocked
 * @SEATON_PREFERENCE_PM_UE: Unknown Error
 * @SEATON_PREFERENCE_PM_DBE: Bad Database
 *
 * Success or error status returned by most libseaton functions.
 */
typedef enum
{
	/*< public >*/
	SEATON_PREFERENCE_PM_OK  =    0,
	SEATON_PREFERENCE_PM_BBE,
	SEATON_PREFERENCE_PM_BDS,
	SEATON_PREFERENCE_PM_KNF,
	SEATON_PREFERENCE_PM_NMS,
	SEATON_PREFERENCE_PM_DSTB,
	SEATON_PREFERENCE_PM_OOR ,
	SEATON_PREFERENCE_PM_BLOCKED,
	SEATON_PREFERENCE_PM_UE ,
	SEATON_PREFERENCE_PM_DBE
} SeatonErrorResults;

#define SEATON_TYPE_PREFERENCE seaton_preference_get_type()

#define SEATON_PREFERENCE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  SEATON_TYPE_PREFERENCE, SeatonPreference))

#define SEATON_PREFERENCE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  SEATON_TYPE_PREFERENCE, SeatonPreferenceClass))

#define SEATON_IS_PREFERENCE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  SEATON_TYPE_PREFERENCE))

#define SEATON_IS_PREFERENCE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  SEATON_TYPE_PREFERENCE))

#define SEATON_PREFERENCE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  SEATON_TYPE_PREFERENCE, SeatonPreferenceClass))

typedef struct _SeatonPreference SeatonPreference;
typedef struct _SeatonPreferenceClass SeatonPreferenceClass;
typedef struct _SeatonPreferencePrivate SeatonPreferencePrivate;

struct _SeatonPreference
{
  GObject parent;

  SeatonPreferencePrivate *priv;
};

struct _SeatonPreferenceClass
{
  GObjectClass parent_class;
};

/**
 * SeatonFieldParam:
 * @pKeyName: Field parameter name
 * @inFieldType: Data type of the field
 *
 * Description of a field parameter in the database.
 */
typedef struct {
	gchar *pKeyName;
	int inFieldType;
} SeatonFieldParam;

GType seaton_preference_get_type (void) G_GNUC_CONST;

SeatonPreference *seaton_preference_new (void);

gint seaton_preference_remove (SeatonPreference *pGobj,gchar *pTableName, const gchar* pKeyStr, gchar *pCondStr);


gint seaton_preference_set (SeatonPreference *pGobj, gchar *pTableName, const gchar *pUniqueKey, gchar *pUniqueValue, const gchar* pKeyStr, gchar *pData);

gint seaton_preference_set_multiple (SeatonPreference *pGobj,gchar *pTableName, GHashTable* hash, gchar *pConditon);

GPtrArray* seaton_preference_get (SeatonPreference *pGobj,gchar *pTableName, const gchar* pKeyStr, gchar *gCond);


GPtrArray* seaton_preference_get_multiple (SeatonPreference *pGobj,gchar *pTableName, gchar *pWhereFrom , gchar *gCond);

gint seaton_preference_add_data (SeatonPreference *pGobj,gchar *pTableName, GHashTable* hash);


gint  seaton_preference_install (SeatonPreference *pGobj, gchar *pTableName, SeatonFieldParam *pfieldparam, gint inFieldSize);


gint seaton_preference_open (SeatonPreference *pGobj, const char *pAppName, const char *pFileName , guint uinDBType);

gint seaton_preference_close (SeatonPreference *pGobj);

gboolean seaton_preference_check_exists (SeatonPreference *pGobj, const gchar *pAppName, const char *pFileName, guint uinDBType);

G_END_DECLS

#endif /* __SEATON_PREFERENCE_H__ */
