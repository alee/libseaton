/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 ============================================================================
 Name        : test.c
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description :Test app in For libseaton
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "seaton-preference.h"
#include <glib/gprintf.h>

gint main(void)
{
	char 			*cAppName = "sqlite";
	int   			ifieldSize = 3;//hardcoded to 2
	GHashTable*		hash = g_hash_table_new(g_str_hash, g_str_equal);
	GPtrArray 		*gpArray;
	GHashTable*     hashChk;
	GList			*list;
	GList 			*ptr;
	int   			iFieldSize;

	int				inVal;
	int 			inResult;
	int				inNumRows;
	int 			inTabSze;
	char			cVal[512];

	//g_type_init();
	/*
	 * Crete the object
	 *
	 */
	SeatonPreference *sqliteObj = seaton_preference_new();

	/*
	* open the database
	*/
	inResult = seaton_preference_open (sqliteObj, cAppName, cAppName, SEATON_PREFERENCE_APP_INTERNAL_DB );
	if( !inResult )
	{
		printf("\nData Base Created...\n");
	}
	else if (inResult)
	{
		printf("\nData Base Creation failed...\n");
	}
	/*
	*
	*/
	SeatonFieldParam pfieldparam[10];
	SeatonFieldParam *pFieldAddColum = malloc(sizeof(SeatonFieldParam));

	g_hash_table_insert(hash, "Text", "Richmond");
	g_hash_table_insert(hash, "width", "720");
	g_hash_table_insert(hash, "height", "470");

	pfieldparam[0].pKeyName = malloc(10);
	pfieldparam[1].pKeyName = malloc(10);
	pfieldparam[2].pKeyName = malloc(10);

	pFieldAddColum->pKeyName = malloc(10);

	g_stpcpy(pFieldAddColum->pKeyName,"FieldA");
	pFieldAddColum->inFieldType = SQLITE_TEXT;


	g_stpcpy(pfieldparam[0].pKeyName,"Text");
	pfieldparam[0].inFieldType= SQLITE_TEXT;

	g_stpcpy((char*)pfieldparam[1].pKeyName,(char*)"width");
	pfieldparam[1].inFieldType= SQLITE_INTEGER;

	g_stpcpy((char*)pfieldparam[2].pKeyName,(char*)"height");
	pfieldparam[2].inFieldType= SQLITE_INTEGER;

	/*
	* create the table
	*/

	inResult = seaton_preference_install(sqliteObj, NULL, pfieldparam, ifieldSize );

	if( !inResult )
	{
		printf("\n Table creation Successful ..\n");
	}
	else if (inResult)
	{
	 	printf("\n Table creation failed...\n");
	 	return 1;//fail
	}

	/*
	*Add the values from the hash table
	*/
	inResult = seaton_preference_add_data (sqliteObj, NULL, hash );



	if( !inResult )
	{
		printf("\n Data Added ..\n");
	}
	else if (inResult)
	{
	 	printf("\n Data Adding Failed ...\n");
	}

	inResult = seaton_preference_add_data (sqliteObj, NULL, hash );

	if( !inResult )
	{
		printf("\n Data Addtion Successful ..\n");
	}
	else if (inResult)
	{
	 	printf("\n Data Addtion failed...\n");
	}
	g_hash_table_insert(hash, "height", "300");
	seaton_preference_set_multiple (sqliteObj, NULL, hash,"ID=1");

	/*
	* free The hash table
	*/
	g_hash_table_destroy(hash);

	/*
	 *  get the  values extracted
	 */

	gpArray = seaton_preference_get (sqliteObj, NULL, "width", "720" );

	if(gpArray == NULL)
	{
		printf("\n Key and value does not exist  \n");
	}
	else
	{
		printf("\n Resulting query is successful The result follows\n");
	}

	inResult = seaton_preference_install (sqliteObj, NULL, pFieldAddColum, 0 );

	if( !inResult )
	{
		printf("\nColumn added ..\n");
	}
	else if (inResult)
	{
	 	printf("\nColumn addition failed...\n");
	}

	inTabSze = gpArray->len;


	for (inNumRows = 0; inNumRows < inTabSze; inNumRows++)
	{
		hashChk = g_ptr_array_index(gpArray , inNumRows);

		iFieldSize = g_hash_table_size(hashChk);

		list = g_hash_table_get_keys (hashChk );

		ptr = list;

		for(inVal = 1; inVal <= iFieldSize; inVal++)
		{
			printf("The Key Name is '%s'\n", (char*)ptr->data);
			g_stpcpy(cVal, g_hash_table_lookup(hashChk, ptr->data));
			printf("The Key Value  '%s'\n",cVal);
			ptr = g_list_nth (list, inVal);
		}
	}

	/*
	 * free The memory after the use
	 *
	 */
	inTabSze = gpArray->len;

	for (inNumRows = 0; inNumRows < inTabSze; inNumRows++)
	{
		hashChk = g_ptr_array_index(gpArray , inNumRows);

		g_hash_table_destroy(hashChk);

	}
	/*
	 * frees the Gpointer
	 */
	 g_ptr_array_free (gpArray , TRUE);

 	/*
 	 *Set the property value
 	*/
	 inResult = seaton_preference_set (sqliteObj, NULL, "ID", "1", "width", "500");
	 if( !inResult )
	 {
		 printf("\n Data Set  Successful ..\n");
	 }
	 else if (inResult)
	 {
		 printf("\n Data Set failed...\n");
	 }

	 inResult = seaton_preference_set (sqliteObj, NULL, 0, 0, "width", "1234");

	 if( !inResult )
	 {
		 printf("\n Data Set  Successful ..\n");
	 }
	 else if (inResult)
	 {
		 printf("\n Data Set failed...\n");
	 }
 	/*
 	* remove the preference
 	*/
	 gpArray = seaton_preference_get_multiple (sqliteObj, NULL, "height", "height=470" );

	if(gpArray == NULL)
	{
		printf("\n Key and value does not exist  \n");
	}
	else
	{
		printf("\n Resulting query is successful The result follows\n");
	}

		inTabSze = gpArray->len;
		for (inNumRows = 0; inNumRows < inTabSze; inNumRows++)
		{
			hashChk = g_ptr_array_index(gpArray , inNumRows);

			iFieldSize = g_hash_table_size(hashChk);

			list = g_hash_table_get_keys (hashChk );

			ptr = list;

			for(inVal = 1; inVal <= iFieldSize; inVal++)
			{
				printf("The Key Name is '%s'\n", (char*)ptr->data);
				g_stpcpy(cVal, g_hash_table_lookup(hashChk, ptr->data));
				printf("The Key Value  '%s'\n",cVal);
				ptr = g_list_nth (list, inVal);
			}
		}

		/*
		 * free The memory after the use
		 *
		 */
		inTabSze = gpArray->len;

		for (inNumRows = 0; inNumRows < inTabSze; inNumRows++)
		{
			hashChk = g_ptr_array_index(gpArray , inNumRows);

			g_hash_table_destroy(hashChk);

		}
		/*
		 * frees the Gpointer
		 */
		 g_ptr_array_free (gpArray , TRUE);


	 inResult = seaton_preference_remove (sqliteObj, NULL, "width", "1234" );

 	if(!inResult )
 	 {
 		 printf("\n Data removed..\n");
 	 }
 	 else if (inResult)
 	 {
 		 printf("\n Data Remove failed...\n");
 	}
	/*
 	* close the Database
 	*/

 	free(pfieldparam[0].pKeyName);
 	free(pfieldparam[1].pKeyName);
 	free(pfieldparam[2].pKeyName);

	seaton_preference_close (sqliteObj);

    return EXIT_SUCCESS;
}
